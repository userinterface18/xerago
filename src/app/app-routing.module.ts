import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ShowfieldsComponent } from './showfields/showfields.component';
import { CreateformfieldsComponent } from './createformfields/createformfields.component';


const routes: Routes = [
  {
    path : 'show',
    component : ShowfieldsComponent
  },
  {
    path: '',
    component : CreateformfieldsComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
