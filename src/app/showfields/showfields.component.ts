import { Component, OnInit } from '@angular/core';
@Component({
  selector: 'app-showfields',
  templateUrl: './showfields.component.html',
  styleUrls: ['./showfields.component.scss']
})
export class ShowfieldsComponent implements OnInit {
  getdatas: string;
  finaldatas: any;

  constructor() { }

  ngOnInit(): void {
    this.getdatas = localStorage.getItem('formcreated');
    this.finaldatas = JSON.parse(this.getdatas);
    console.log(this.finaldatas);
  }

}
