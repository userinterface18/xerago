import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateformfieldsComponent } from './createformfields.component';

describe('CreateformfieldsComponent', () => {
  let component: CreateformfieldsComponent;
  let fixture: ComponentFixture<CreateformfieldsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateformfieldsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateformfieldsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
