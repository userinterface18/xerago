import { Component, AfterViewInit, ElementRef, ViewChild, OnInit } from '@angular/core';
import { CdkDragDrop, moveItemInArray, transferArrayItem } from '@angular/cdk/drag-drop';
import { EventEmitter } from 'events';
import { ApiService } from '../services/api.service';
@Component({
  selector: 'app-createformfields',
  templateUrl: './createformfields.component.html',
  styleUrls: ['./createformfields.component.scss']
})


export class CreateformfieldsComponent implements OnInit {
  results: any = [];
  addeddatas: string[];

  constructor(private api: ApiService) {

  }
  todo = [];

  done = [];
  ngOnInit() {
    this.api.getforms().subscribe(response => {
      this.results = response;
      this.todo = this.results.forms;
    });
  }
  drop(event: CdkDragDrop<string[]>) {
    if (event.previousContainer === event.container) {
      moveItemInArray(event.container.data, event.previousIndex, event.currentIndex);
    } else {
      transferArrayItem(event.previousContainer.data,
        event.container.data,
        event.previousIndex,
        event.currentIndex);
    }
    this.addeddatas = this.done;
    localStorage.setItem('formcreated', JSON.stringify(this.addeddatas));
  }
}
