import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ApiService {
  public basurl = '../assets/jsondatas/';
  constructor(private http: HttpClient) { }
  getforms() {
    return this.http.get(this.basurl + 'form.json');
  }
}
